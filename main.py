#!/usr/bin/env python3

import subprocess
import argparse

from threading import Thread
import time
import re
from curses import wrapper
import math
import csv
import sys

import requests #for gotify

class Host(Thread):
    PING_DELAY_S=1
    PING_MEAN_SAMPLES=6
    def __init__(self, address, name=None, observers=[]):
        Thread.__init__(self)
        self.name = name
        self.address = address
        self.exited = False
        self.observers = observers
        self.start()
        self.ms = -1
        self.max_ms = -1
        self.min_ms = 100000
        self.lasts_ms = []
        self.disconnect_count = 0
        self.connected = None

    def get_name(self, force=False):
        #TODO why str class?!
        if self.name != 'None':
            return self.name
        if force:
            return self.address
    
    def on_connect(self):
        for o in self.observers:
            o.on_connect(self.address, self.name)

    def on_disconnect(self):
        for o in self.observers:
            o.on_disconnect(self.address, self.name)

    def run(self):
        while not self.exited:
            res = self.ping()
            if res < 0:
                if self.connected or self.connected is None:
                    self.connected = False
                    self.disconnect_count += 1
                    self.on_disconnect()
            else:
                if not self.connected:
                    self.connected = True
                    self.on_connect()
            self.ms = res
            self.max_ms = max(self.max_ms, self.ms)
            self.min_ms = min(self.min_ms, self.ms)
            if self.ms > 0:
                self.lasts_ms.append(self.ms)
                if len(self.lasts_ms) > Host.PING_MEAN_SAMPLES:
                    self.lasts_ms = self.lasts_ms[-Host.PING_MEAN_SAMPLES:]
            time.sleep(Host.PING_DELAY_S)

    def stop(self):
        self.exited=True

    def mean(self):
        if len(self.lasts_ms) < Host.PING_MEAN_SAMPLES:
            return -1
        if not self.connected:
            return -1
        s = 0
        for l in self.lasts_ms:
            s += l
        return s/len(self.lasts_ms)

    def stats(self):
        return self.ms, self.mean(), [self.min_ms, self.max_ms]

    def ping(self):
        cmd = ["/bin/ping", "-c1", "-w20", self.address]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        status = process.wait()
        if status == 1:
            return -1
        else:
            res = process.stdout.read().decode('utf8')
            lines = res.split('\n')
            if len(lines) > 1:
                r = re.compile(r'.*time=(.+)\s.*')
                data = r.match(lines[1]).groups()
                return float(data[0])
        return -1

class HostsPool:
    def __init__(self, observers):
        self.hosts = []
        self.min = 10000
        self.max = -1
        self.observers = observers

    def append(self, address, name=None):
        self.hosts.append(Host(address, name, self.observers))

    def get_ping_range_ms(self):
        min_ms = 100000
        max_ms = -1
        for h in self.hosts:
            min_ms = min(min_ms, h.min_ms)
            max_ms = max(max_ms, h.max_ms)
        return (min_ms,  max_ms)

    def remap(self, new_range, i):
        min_ms, max_ms = self.get_ping_range_ms()
        initial_range_ms = max_ms - min_ms
        if initial_range_ms > 0:
            return new_range[0] + (i - min_ms) * (new_range[1]-new_range[0]) / initial_range_ms
        return 0


class NcurseDisplay(Thread):
    DISP_MIN = 0
    DISP_MAX = 50

    def __init__(self, stdsr, pool):
        Thread.__init__(self)
        self.pool = pool
        self.stdscr = stdsr
        self.running = False
        self.start()

    def stop(self):
        self.running = False

    def format_number(self, i):
        if i > -1:
            return '{: >7.3f}'.format(i)
        else:
            return ' '*7

    def remap(self, i):
        return self.pool.remap([self.DISP_MIN, self.DISP_MAX], i)

    def run(self):
        display_index = 0
        display_mode = 0
        self.running = True
        while(self.running):
            self.stdscr.clear()

            #display header
            self.stdscr.addstr(0, 0, '\tAddress\t\t  ping\t     E\t\tmean\t\t  graph')
            # toggle address or custom name display
            if display_index%10==0:
                if display_mode == 1:
                    display_mode = 0
                else:
                    display_mode = 1
                    
            # for each host
            for i,h in enumerate(self.pool.hosts):
                #compute values
                ms, mean_ms, range_ms = h.stats()

                ms_remap = self.remap(ms)-1
                mean_remap = self.remap(mean_ms)-1
                max_remap = self.remap(range_ms[1])-1
                index = int(math.ceil(ms_remap))
                
                #build progress bar
                progress = ['|']*index + [' ']*(self.DISP_MAX-index) + [' ']
                if mean_remap > 0:
                    progress[int(math.ceil(mean_remap))] = '¦'
                if max_remap > 0:
                    progress[int(math.ceil(max_remap))] = '#'
                
                # set host display title
                display = h.address
                if display_mode == 1:
                    display = h.get_name(True)
                
                maxsize = 15
                if len(display) > maxsize:
                    display = display[:maxsize-3] + "..."
                
                # display host row
                self.stdscr.addstr(i+2, 0, '{: >15}\t\t{}\t{:>3}\t\t{}\t\t{}'.format(display,self.format_number(h.ms), h.disconnect_count, self.format_number(h.mean()), ''.join(progress)))
                
            self.stdscr.refresh()
            #stdscr.getkey()
            time.sleep(0.5)


class CsvWriter(Thread):
    def __init__(self, pool, outfile):
        Thread.__init__(self)
        self.running = False
        self.pool = pool

        if outfile == 'stdout':
            self.outfile = sys.stdout
        else:
            self.outfile = open(outfile, "w")
        self.start()

    def run(self):
        self.running = True
        while self.running:
            for h in self.pool.hosts:
                self.outfile.write('{},{},{},{}\n'.format(int(time.time()), h.name, h.address, h.ms))
            time.sleep(0.5)
        self.outfile.close()

    def stop(self):
        self.running = False

class Observer:
    def on_connect(self, address, name):
        pass
    
    def on_disconnect(self, address, name):
        pass

class Gotify(Observer):
    def __init__(self, uri):
        Observer.__init__(self)
        self.uri = uri

    def notify(self, title, message):
        resp = requests.post(self.uri, json={
            "message": message,
            "priority": 20,
            "title": title
        })


    def on_connect(self, address, name):
        n=address
        if name is not None:
            n+=' %s'%name
        self.notify(n, "Host %s is now up"%n)

    def on_disconnect(self, address, name):
        n=address
        if name is not None:
            n+=' %s'%name
        self.notify(n, "Host %s is now up"%n)


if __name__ == '__main__':
    def parse_args():
        parser = argparse.ArgumentParser()
    
        parser.add_argument("-a", "--address",help="address to ping (ie google.fr:wan 192.168.0.1:gw", action='append', nargs='*')
        parser.add_argument("-c", "--config",help="config file")
        parser.add_argument("-g", "--gotify",help="gotify app token")
        parser.add_argument("-w", "--write",help="write to file (you can use stdout)", default=None)
        parser.add_argument("-x", "--ncurse",help="ncurse visualisation",
                action="store_true", default=False)
    
        args = parser.parse_args()
    
        return args
    
    def load_config_file(path):
        addresses = []
        with open(path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                addresses.append(row[0])
        return addresses
    
    def main(stdscr, args):
        addresses = []
        if args.address is not None:
            addresses = args.address[0]
        if args.config is not None:
            addresses += load_config_file(args.config)
  
        observers = []
        if args.gotify is not None:
            observers.append(Gotify(args.gotify))

        pool = HostsPool(observers)
       
        writer = None
        ncurse = None
        if args.write is not None:
            writer = CsvWriter(pool, args.write)
        if args.ncurse:
            ncurse = NcurseDisplay(stdscr, pool)
        

        for address in addresses:
            name = None
            r = address.split(':')
            if len(r) == 2:
                address = r[0]
                name = r[1]
            pool.append(address, name)
        try:
            while True:
                time.sleep(0.5)
        except KeyboardInterrupt:
            pass
        if writer is not None:
            writer.stop()
            writer.join()
        if ncurse is not None:
            ncurse.stop()
            ncurse.join()
        for h in pool.hosts:
            h.stop()
        for h in pool.hosts:
            h.join()
    
    
    # parse command line
    args = parse_args()
    if args.ncurse:
        wrapper(main, args)
    else:
        main(None, args)
      
